import pywhatkit as pywhatkit
import speech_recognition as sr
import os
import time

def get_audio():
    try:
        recorder = sr.Recognizer()
        with sr.Microphone() as source:
            print("Say something: ")
            audio = recorder.listen(source)
        text = recorder.recognize_google(audio, language="en-EN")
        print("[log] Recognized: " + text)
        point = []
        for item in text.lower().split(' '):
            point.append(item)
        return point
    except sr.UnknownValueError:
        print("[log] Not Recognized!")
    except sr.RequestError as e:
        print("[log] Unknown error, check the internet!")

def try12(cmd):
    if not cmd:
        return 'No orders'
    else:
        if 'telegram' in cmd:
            return os.startfile("path to file")
        elif 'thompson' in cmd:
            return os.startfile("path to file")
        elif 'time' in cmd:
            return os.startfile("path to file")
        elif 'youtube' in cmd:
            return pywhatkit.playonyt(cmd)
        elif 'google' in cmd:
            return pywhatkit.search(cmd)

while True:
    try12(get_audio())
    time.sleep(0.2)

