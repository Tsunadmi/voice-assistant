# Voice-assistant

Video fase recognizer

## Installation

You need install python 3.9:
```
pip install python
```
## Virtual environment

In terminal in project folder you should activate virtual environment:

```
venv/Scripts/activate
```

## Add important libraries

Needed libraries:

```
SpeechRecognition==3.8.1
pyttsx3==2.90
pytweening==1.0.4
pywhatkit==5.4
pywin32==304
pypiwin32==223
PyAudio==0.2.12

```

## Getting started

Start app:
```
Voice_bot.py
```
## Technologies used
 
- Python
- SpeechRecognition

## Description

Voice assistant can open any application/folder/program with your voice. 
Also, use your voice script can find all need information on Google or just find any videos on YouTube for relaxation.

## Author

This app was done by Dmitry Tsunaev.

- [ ] [LinkedIn](http://linkedin.com/in/dmitry-tsunaev-530006aa)

